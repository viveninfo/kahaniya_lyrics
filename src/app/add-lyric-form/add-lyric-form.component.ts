import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgForm, FormsModule } from '@angular/forms';
import { Http, Response } from '@angular/http';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule, MatFormFieldModule, MatButtonModule,
  MatSelectModule, MatOptionModule } from '@angular/material';

import { Lyric } from '../shared/lyric.interface';
import { ApiService } from './../services/api.service';
import {  } from '@angular/router/src/router_state';

@Component({
  selector: 'app-add-lyric-form',
  templateUrl: './add-lyric-form.component.html',
  styleUrls: ['./add-lyric-form.component.scss']
})

export class AddLyricFormComponent implements OnInit {
  
  @ViewChild('f') lyricForm: NgForm;
  songId: string = '';
  currentLyric: Lyric;

  constructor(private api: ApiService) { }

  ngOnInit() { }

  saveNewLyric(f: NgForm) {

    let lyric = {} as Lyric;
    lyric.ttl = f.value['ttl'];
    lyric.tid = f.value['tid'];
    lyric.telugu = f.value['telugu'];
    lyric.english = f.value['english'];
    lyric.author = f.value['author'];
    lyric.summary = f.value['summary'];
    lyric.img = f.value['img'];
    lyric.authorId = f.value['authorId'];

    this.api.saveLyrics(lyric).subscribe(
      (res: Response) => {
        console.log(res);
        this.lyricForm.reset();
    });
  }

  fetchData() {
    this.currentLyric = {} as Lyric;
    this.api.getLyricFromId(this.songId).subscribe(
      (data: Response) => {
        console.log(data.json());
        this.currentLyric.ttl = data.json().ttl;
        this.currentLyric.tid = data.json().tid;
        this.currentLyric.telugu = data.json().telugu;
        this.currentLyric.english = data.json().english;
        this.currentLyric.author = data.json().author;
        this.currentLyric.summary = data.json().summary;
        this.currentLyric.img = '';
        this.currentLyric.authorId = '';

        this.lyricForm.setValue({
          ttl: this.currentLyric.ttl,
          tid: this.currentLyric.tid,
          img: this.currentLyric.img,
          telugu: this.currentLyric.telugu,
          english: this.currentLyric.english,
          author: this.currentLyric.author,
          authorId: this.currentLyric.authorId,
          summary: this.currentLyric.summary
        })
      }
    );
  }

}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatFormFieldModule, 
    MatSelectModule, 
    MatOptionModule,
    MatButtonModule
  ],
  exports: [
    AddLyricFormComponent
  ],
  declarations: [
    AddLyricFormComponent
  ]
})

export class AddLyricsModule {

}