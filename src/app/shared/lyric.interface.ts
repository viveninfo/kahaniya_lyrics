export interface Lyric {
    ttl: string, 
    tid: string, 
    img: string,
    telugu: string, 
    english: string, 
    summary: string,
    author: string, 
    authorId: string
}