import { Lyric } from './../shared/lyric.interface';
import { Injectable } from '@angular/core';

import { Http, Response } from '@angular/http';

@Injectable()
export class ApiService {

  currentLyric: Lyric;
  apiUrl = 'https://protoapi.kahaniya.com/';
  constructor(private http: Http) {}

  getLyricFromId(id: string) {
    return this.http.post(this.apiUrl + 'gtlyrics', {'id': id});
  }

  saveLyrics(lyric: Lyric) {
    return this.http.post(this.apiUrl + 'updtlyrics', lyric);
  }

}
